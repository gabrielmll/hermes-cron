package main

import (
	"fmt"
	"log"
	"os"
	"residuall/heaven/db"

	"bitbucket.org/liamstask/goose/lib/goose"

	"gopkg.in/guregu/null.v3"

	"github.com/jmoiron/sqlx"
)

// ClientsInactivity returns inactivity status of every client and the total
type ClientsInactivity struct {
	Generator        int64      `db:"generator"`
	NumConsideredCRq null.Int   `json:"num_considered_crq" db:"num_considered_crq"`
	LastCRq          null.Time  `json:"last_crq" db:"last_crq"`
	DaysPassed       null.Int   `json:"days_passed" db:"days_passed"`
	DaysTOBEInactive null.Float `json:"days_tobe_inactive" db:"days_tobe_inactive"`
	IsInactive       null.Int   `json:"is_inactive" db:"is_inactive"`
	DaysInactive     null.Float `json:"days_inactive" db:"days_inactive"`
}

func DBConnect() {
	var err error
	var openstr string
	driver := "mysql"
	db_name := os.Getenv("HEAVEN_DB_NAME")
	db_user := os.Getenv("HEAVEN_DB_USER")
	db_pass := os.Getenv("HEAVEN_DB_PASS")
	db_host := os.Getenv("HEAVEN_DB_HOST")
	db_port := os.Getenv("HEAVEN_DB_PORT")

	if db_user == "" {
		env := os.Getenv("HEAVEN_DBENV")
		if env == "" {
			env = "development"
		}
		dbconf, err := goose.NewDBConf("db", env, "")
		if err != nil {
			panic(err)
			return
		}

		driver = dbconf.Driver.Name
		openstr = dbconf.Driver.OpenStr
	} else {
		if db_host == "" {
			db_host = "127.0.0.1"
		}
		if db_port == "" {
			db_port = "3306"
		}
		if db_name == "" {
			db_name = "heaven"
		}

		openstr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
			db_user, db_pass, db_host, db_port, db_name)
	}

	db.Conn, err = sqlx.Connect(driver, openstr)
	if err != nil {
		panic(err)
		return
	}
}

// GeneratorsInactivity returns GensInfo
func GeneratorsInactivity(tx *sqlx.Tx) {

	tx.MustExec(`SET @num := 0, @type := 0`)

	tx.MustExec(`
		CREATE TEMPORARY TABLE IF NOT EXISTS _aux_last_crqs
		  (SELECT crq.generator_entity_id AS generator,
		          DATE(crq.created_at) AS crq_created_at,
		          DATE(e.created_at) AS e_created_at,
		          crq.id AS crq_id,
		          crq.collector_entity_id AS collector
		   FROM collection_requests crq
		   INNER JOIN entities e ON e.id = crq.generator_entity_id
		   ORDER BY e.id,
		            crq.created_at DESC)`)

	tx.MustExec(`
		CREATE TEMPORARY TABLE IF NOT EXISTS tmp_last_crqs
		SELECT crq_id,
		       collector,
		       generator,
		       IF(crq_id IS NULL,
		                    0,
		                    crq_number) AS crq_number,
		       IF(crq_id IS NULL,
		                    e_created_at,
		                    crq_created_at) AS created_at
		FROM
		  (SELECT collector,
		          generator, @num := IF(@type = generator, @num + 1, 1) AS crq_number, @type := generator AS gen_id, crq_created_at,
		                                                                                                             crq_id,
		                                                                                                             e_created_at
		   FROM _aux_last_crqs
		   ORDER BY generator,
		            crq_created_at DESC) _last_crqs`)

	tx.MustExec(`
		CREATE TEMPORARY TABLE IF NOT EXISTS tmp_clients_info
		  (SELECT _inactivity.generator,
		          _inactivity.num_considered_crq,
		          _inactivity.last_crq,
		          _inactivity.days_passed,
		          _inactivity.days_tobe_inactive,
		          _inactivity.is_inactive,
		          _inactivity.days_inactive
		   FROM
		     (SELECT generator,
		             num_considered_crq,
		             last_crq,
		             days_passed,
		             days_tobe_inactive,
		             if(num_considered_crq IN (0, 1), 0, if(days_passed - days_tobe_inactive > 0, 1, 0)) AS is_inactive,
		             days_passed - days_tobe_inactive AS days_inactive
		      FROM
		        (SELECT generator,
		                max(crq_number) AS num_considered_crq,
		                max(created_at) AS last_crq,
		                datediff(now(), max(created_at)) AS days_passed,
		                round(datediff(max(created_at), min(created_at))/(max(crq_number)-1)*1.2) AS days_tobe_inactive
		         FROM tmp_last_crqs
		         WHERE crq_number <= 4
		         GROUP BY generator) AS _tbl_inactivity) AS _inactivity)`)

	tx.MustExec(`
		UPDATE entities e
		INNER JOIN tmp_clients_info tmp ON e.id = tmp.generator
		SET e.is_inactive = tmp.is_inactive,
		    e.days_inactive = tmp.days_inactive`)

	tx.MustExec(`DROP TEMPORARY TABLE IF EXISTS _aux_last_crqs`)
	tx.MustExec(`DROP TEMPORARY TABLE IF EXISTS tmp_last_crqs`)
	tx.MustExec(`DROP TEMPORARY TABLE IF EXISTS tmp_clients_info`)
}

func main() {

	logFile, err := os.OpenFile("log-hermes-cron", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}

	defer logFile.Close()

	// direct all log messages to log.txt
	log.SetOutput(logFile)
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	DBConnect()
	defer db.Conn.Close()

	log.Println("Executing queries")
	tx := db.Conn.MustBegin()
	GeneratorsInactivity(tx)
	log.Println("Finished queries")
	tx.Commit()
	log.Println("-----------------------------------------")
}
