
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE `user_tokens` (
	`token` char(47) NOT NULL,
	`user_id` int(11) NOT NULL,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

	CONSTRAINT pk_user_tokens_id PRIMARY KEY (token),
	CONSTRAINT `fk_user_tokens_user_id`
			FOREIGN KEY (user_id) REFERENCES users (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE IF EXISTS user_tokens;
