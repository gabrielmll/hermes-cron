-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: 172.17.0.2    Database: heaven
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.11-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `business_groups`
--

LOCK TABLES `business_groups` WRITE;
/*!40000 ALTER TABLE `business_groups` DISABLE KEYS */;
INSERT INTO `business_groups` VALUES (1,'Estabelecimentos Alimentícios','Estabelecimentos de consumo de produtos alimentício.');
/*!40000 ALTER TABLE `business_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `business_type`
--

LOCK TABLES `business_type` WRITE;
/*!40000 ALTER TABLE `business_type` DISABLE KEYS */;
INSERT INTO `business_type` VALUES (1,'Churrascaria',NULL),(2,'Pizzaria',NULL),(3,'Cafeteria / Confeitaria',NULL),(4,'Padaria',NULL),(5,'Bares / Botecos',NULL),(6,'Cozinha Brasileira',NULL),(7,'Cozinha Japonesa',NULL),(8,'Cozinha Italiana',NULL),(9,'Cozinha Chinesa',NULL),(10,'Cozinha Árabe',NULL),(11,'Cozinha Internacional',NULL),(12,'Cozinha Natural / Vegana',NULL),(13,'Self-service',NULL),(14,'Lanchonete / Fast-food',NULL);
/*!40000 ALTER TABLE `business_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `business_type_groups`
--

LOCK TABLES `business_type_groups` WRITE;
/*!40000 ALTER TABLE `business_type_groups` DISABLE KEYS */;
INSERT INTO `business_type_groups` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1);
/*!40000 ALTER TABLE `business_type_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `entities`
--

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
INSERT INTO `entities` VALUES (1,'Residuall','contato@residuall.com','993430928','Av. Flávio dos Santos','372','31015150','Belo Horizonte','Mi','Brasil','100000000',0,0,NULL,NULL,NULL,1,1,NULL,NULL);
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `entities_residue_types`
--

LOCK TABLES `entities_residue_types` WRITE;
/*!40000 ALTER TABLE `entities_residue_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `entities_residue_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `entity_types`
--

LOCK TABLES `entity_types` WRITE;
/*!40000 ALTER TABLE `entity_types` DISABLE KEYS */;
INSERT INTO `entity_types` VALUES (1,'Residuall','A melhor startup do universo.'),(2,'Empresa de Coleta','Empresas de coleta de resíduos.'),(3,'Gerador de Resíduos','Entidades que geram resíduos.');
/*!40000 ALTER TABLE `entity_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `invitations`
--

LOCK TABLES `invitations` WRITE;
/*!40000 ALTER TABLE `invitations` DISABLE KEYS */;
INSERT INTO `invitations` VALUES (1,1,1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `request_residue_types`
--

LOCK TABLES `request_residue_types` WRITE;
/*!40000 ALTER TABLE `request_residue_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_residue_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `residue_type`
--

LOCK TABLES `residue_type` WRITE;
/*!40000 ALTER TABLE `residue_type` DISABLE KEYS */;
INSERT INTO `residue_type` VALUES (1,'Resíduos dos serviços de saúde',''),(2,'Óleos de motores, transmissões e lubrificação usados ou contaminados',''),(3,'Óleo Vegetal usado',''),(4,'Óleos hidráulicos usados',''),(5,'Plástico',''),(6,'Alumínio',''),(7,'Óleos isolantes, de refrigeração e de transmissão de calor usados',''),(8,'Lodos do tratamento local de efluentes',''),(9,'Chumbo',''),(10,'Resíduos Químicos (contempla resíduos de ácidos, bases, sais, farmaceuticos, fertilizantes, etc.)',''),(11,'Aparas de matérias plásticas',''),(12,'Absorventes, materiais filtrantes, panos de limpeza e vestuário de proteção',''),(13,'Níquel',''),(14,'Resíduos Orgânicos',''),(15,'Resíduos de equipamento elétrico e eletrônico',''),(16,'Resíduos de combustíveis líquidos',''),(17,'Resíduos da refinação de petróleo',''),(18,'Escórias do forno',''),(19,'Óleos bunker usados de navios',''),(20,'Embalagens de papel e cartão',''),(21,'Aparas e limalhas de metais',''),(22,'Ferro e aço',''),(23,'Estanho',''),(24,'Cobre, bronze e latão',''),(25,'Pilhas, baterias e acumuladores elétricos:',''),(26,'Zinco',''),(27,'Resíduos da produção de bebidas alcoólicas e não alcoólicas (excluindo café, chá e cacau)',''),(28,'Vidro',''),(29,'Resíduos retirados da fase de gradeamento',''),(30,'Resíduos de processos térmicos',''),(31,'Pneus',''),(32,'Sucatas metálicas',''),(33,'Resíduos da agricultura, horticultura, aquicultura, silvicultura, caça e pesca',''),(34,'Resíduos de instalações de gestão de resíduos, de estações de tratamento de águas residuais e da preparação de água para consumo humano e água para consumo industrial:',''),(35,'Resíduos da transformação física e química de minérios',''),(36,'Resíduos da indústria do couro e produtos de couro e da indústria têxtil',''),(37,'Resíduos da mineração',''),(38,'Resíduos de construção e demolição (incluindo solos escavados de locais contaminados):',''),(39,'Madeira',''),(40,'Resíduos de tecidos animais',''),(41,'Resíduos do processamento de madeira e fabricação de painéis e mobiliário',''),(42,'Resíduos de processos de galvanização',''),(43,'Resíduos da indústria do ferro e do aço',''),(44,'Embalagens longa-vida',''),(45,'Resíduos da preparação e processamento de carne, peixe e outros produtos alimentares de origem animal',''),(46,'Tijolos',''),(47,'Resíduos de tratamentos químicos de superfície e revestimentos de metais e outros materiais (galvanização, zincagem, decapagem, contrastação, fosfatação, desengorduramento alcalino, anodização)',''),(48,'Ladrilhos, telhas e materiais cerâmicos',''),(49,'Resíduos da fabricação do vidro e de produtos de vidro',''),(50,'Resíduos de solventes, fluidos de refrigeração e gases propulsores de espumas/ aerossóis orgânicos',''),(51,'Resíduos de alumina',''),(52,'Resíduos da fabricação de peças cerâmicas, tijolos, ladrilhos, telhas e produtos de construção',''),(53,'Tecidos',''),(54,'Resíduos vitrificados e resíduos da vitrificação:',''),(55,'Resíduos de cimento e de lodos de cimento',''),(56,'Resíduos de tecidos vegetais','');
/*!40000 ALTER TABLE `residue_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador','Administrador da empresa'),(2,'Gestor','Gestor designado pelo Administrador.'),(3,'Operador','Operador da empresa');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `signup_requests`
--

LOCK TABLES `signup_requests` WRITE;
/*!40000 ALTER TABLE `signup_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `signup_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'STATE_WAITING_AUTHORIZATION','Aguardando autorização da Residuall.');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-05 17:53:51
