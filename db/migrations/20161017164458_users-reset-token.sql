
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `users`
  ADD COLUMN `reset_token` VARCHAR(100) AFTER `password`;

ALTER TABLE `users`
  ADD COLUMN `reset_token_exp` timestamp DEFAULT 0 AFTER `reset_token`;

ALTER TABLE `users`
  ADD CONSTRAINT `uq_reset_token` UNIQUE (`reset_token`);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `users`
  DROP COLUMN `reset_token`;

ALTER TABLE `users`
  DROP COLUMN `reset_token_exp`;
