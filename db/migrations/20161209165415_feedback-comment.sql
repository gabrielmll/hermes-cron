
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE feedback_registrations
  ADD `comment` text AFTER `feedback_id`;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE feedback_registrations
  DROP COLUMN `comment`;
