
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

-- --------------------------------------------------------
--
-- Dumping data for table `states`
--
LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (10001,'finished','Everything about this task is done.'),
(11001, 'unallocated', 'Can be allocated to a route_schedule.'),
(11002, 'allocated', 'Allocated to a scheduled route_schedule.'),
(12001, 'waiting_execution', 'Waiting collection to happen.'),
(12002, 'waiting_info', 'Waiting input information.'),
(13001, 'schedulling', 'Being planned. Awaits confirmation.'),
(13002, 'schedulled', 'Already planned. Awaits execution.'),(13003,'in_execution','Being executed.');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

-- --------------------------------------------------------
--
-- Table structure for table `feedback_types`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `feedback_types`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `feedback_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feedback_source` tinyint(1) NOT NULL,
  `feedback_option` text(255) NOT NULL,
  `weight` int(3) NOT NULL,
  CONSTRAINT `pk_feedback_types` PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
--
-- Table structure for table `feedback_registrations`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `feedback_registrations`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `feedback_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_registration_id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  CONSTRAINT `pk_feedback_registrations` PRIMARY KEY (`id`),
  CONSTRAINT `fk_collection_feedback`
    FOREIGN KEY (`collection_registration_id`)
    REFERENCES collection_registrations(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_feedback_registration`
    FOREIGN KEY (`feedback_id`)
    REFERENCES feedback_types(`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Dumping data for table `feedback_types`
--
LOCK TABLES `feedback_types` WRITE;
/*!40000 ALTER TABLE `feedback_types` DISABLE KEYS */;
INSERT INTO `feedback_types` VALUES (1001,'O coletor não apareceu.','The collector didnt show up for a scheduled collection.', 90),
(1002, 'O coletor apareceu fora do horário combinado.', 'The collector appeared outside the agreed time.', 45),
(1003, 'O coletor sujou o ambiente.', 'The collector dirtied the environment.', 90),
(1004, 'Não houve a entrega do recibo.', 'The collector didnt give the receipt.', 30),
(1005, 'Não houve pagamento.', 'The collector didnt pay.', 90),
(2001,'O gerador estava fechado.','The establishment was closed.', 90),
(2002,'Recipiente em local impróprio.','The container is placed inappropriately.', 45),
(2003,'Recipiente danificado.','The container is damaged.', 60),
(2004,'Volume da bombona baixo.','The volume collected is too low.', 45);
/*!40000 ALTER TABLE `feedback_types` ENABLE KEYS */;
UNLOCK TABLES;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM `states`;

DROP TABLE IF EXISTS `feedback_types`;
DROP TABLE IF EXISTS `feedback_registrations`;
SET FOREIGN_KEY_CHECKS=1;
