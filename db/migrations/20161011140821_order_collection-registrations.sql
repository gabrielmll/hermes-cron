
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `collection_registrations`
  ADD COLUMN `ordenation` int(11) NOT NULL AFTER `state_id`;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `collection_registrations`
  DROP COLUMN `ordernation`;
