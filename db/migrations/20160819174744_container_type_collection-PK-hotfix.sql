
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `container_type_collection`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `container_type_collection`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `container_type_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_type_id` int(11) NOT NULL,
  `collection_request_id` int(11) NOT NULL,
  `filled_percentage_requested` int(3),
  `filled_percentage_collected` int(3),
  CONSTRAINT `pk_container_type_collection` PRIMARY KEY(id),
  CONSTRAINT `fk_container_type`
    FOREIGN KEY (`container_type_id`)
    REFERENCES waste_container_types (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_collection_request`
    FOREIGN KEY (`collection_request_id`)
    REFERENCES collection_requests (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
