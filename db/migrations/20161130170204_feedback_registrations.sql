
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `feedback_registrations`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `feedback_registrations`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `feedback_registrations` (
  `entity_type_id` int(11) NOT NULL,
  `collection_registration_id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  CONSTRAINT `pk_feedback_registrations` PRIMARY KEY (`entity_type_id`, `collection_registration_id`),
  CONSTRAINT `fk_collection_feedback`
    FOREIGN KEY (`collection_registration_id`)
    REFERENCES collection_registrations(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_feedback_registration`
    FOREIGN KEY (`feedback_id`)
    REFERENCES feedback_types(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_feedback_entity_type`
    FOREIGN KEY (`entity_type_id`)
    REFERENCES entity_types(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `feedback_registrations`;
SET FOREIGN_KEY_CHECKS=1;
