
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied


CREATE TABLE `business_groups` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_business_groups_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `business_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_business_types_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `entity_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_entity_type_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `entities` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`entity_type_id` int(11) NOT NULL,
	`business_type_id` int(11),
	`business_group_id` int(11),
	`name` varchar(100) NOT NULL,
	`email` varchar(250) DEFAULT NULL,
	`phone` varchar(25) DEFAULT NULL,
	`street_address` varchar(250) NOT NULL,
	`street_number` varchar(10) NOT NULL,
	`postal_code` varchar(20) NOT NULL,
	`city` varchar(45) NOT NULL,
	`state` varchar(2) NOT NULL,
	`country` varchar(45) NOT NULL,
	`br_cpf_cnpj` varchar(14) NOT NULL,
	`latitude` double NOT NULL,
	`longitude` double NOT NULL,
	`logo` varchar(255),
	`primary_color` varchar(6) DEFAULT NULL,
	`secondary_color` varchar(6) DEFAULT NULL,
	`score` int(3) NOT NULL DEFAULT 50,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	UNIQUE(email),
	UNIQUE(br_cpf_cnpj),
	CONSTRAINT `pk_entities_id` PRIMARY KEY (id),
	CONSTRAINT `fk_entities_entity_type_id`
			FOREIGN KEY (entity_type_id) REFERENCES entity_types (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_entities_business_type_id`
			FOREIGN KEY (business_type_id) REFERENCES business_types (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `invitations` (
	`id` varchar(36) NOT NULL,
	`src_entity_id` int(11) NOT NULL,
	`dst_entity_id` int(11) NOT NULL,
	`state_id` int(11) NOT NULL,
	`exp_date` datetime DEFAULT 0,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

	CONSTRAINT `pk_invitations_id` PRIMARY KEY (id),
	CONSTRAINT `fk_invitations_src_entity_id`
			FOREIGN KEY (src_entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_invitations_dst_entity_id`
			FOREIGN KEY (dst_entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_invitations_state_id`
		FOREIGN KEY (state_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `residue_types` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_residue_types_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `roles` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_roles_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `states` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,
	`description` varchar(250) DEFAULT NULL,

	CONSTRAINT `pk_states_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `signup_requests` (
	`id` varchar(36) NOT NULL,
	`entity_type_id` int(11) NOT NULL,
	`business_type_id` int(11) NULL,
	`business_group_id` int(11) NULL,
	`state_id` int(11) NOT NULL,
	`contact_name` varchar(100) NOT NULL,
	`contact_email` varchar(100) NOT NULL,
	`contact_phone` varchar(25) NOT NULL,
	`company_name` varchar(100) NOT NULL,
	`br_cpf_cnpj` varchar(14) NOT NULL,
	`entity_type_description` varchar(250) DEFAULT NULL,
	`other_business` varchar(45) DEFAULT NULL,
	`other_residue` varchar(45) DEFAULT NULL,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

	UNIQUE(contact_email),
	UNIQUE(br_cpf_cnpj),
	CONSTRAINT `pk_signup_requests_id` PRIMARY KEY (id),
	CONSTRAINT `fk_signup_requests_state_id`
			FOREIGN KEY (state_id) REFERENCES states (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_signup_requests_business_type_id`
			FOREIGN KEY (business_type_id) REFERENCES business_types (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_signup_requests_entity_type_id`
			FOREIGN KEY (entity_type_id) REFERENCES entity_types (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_signup_requests_business_group_id`
			FOREIGN KEY (business_group_id) REFERENCES business_groups (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`entity_id` int(11) NOT NULL,
	`role_id` int(11) NOT NULL,
	`state_id` int(11) NOT NULL,
	`name` varchar(100) NOT NULL,
	`email` varchar(100) DEFAULT NULL,
	`password` varchar(100) NOT NULL,
	`phone` varchar(25) DEFAULT NULL,
	`br_cpf` varchar(11) NOT NULL,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	UNIQUE(email),
	UNIQUE(br_cpf),
	CONSTRAINT `pk_users_id` PRIMARY KEY (id),
	CONSTRAINT `fk_users_entity_id`
			FOREIGN KEY (entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_users_role_id`
			FOREIGN KEY (role_id) REFERENCES roles (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_state_id`
			FOREIGN KEY (state_id) REFERENCES states (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `business_types_groups` (
	`business_type_id` int(11) NOT NULL,
	`business_group_id` int(11) NOT NULL,

	CONSTRAINT `pk_business_types_groups` PRIMARY KEY(business_type_id, business_group_id),
	CONSTRAINT `fk_btbg_business_type_id`
			FOREIGN KEY (business_type_id) REFERENCES business_types (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_btbg_business_group_id`
			FOREIGN KEY (business_group_id) REFERENCES business_groups (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `entities_residue_types` (
	`entity_id` int(11) NOT NULL,
	`residue_type_id` int(11) NOT NULL,

	CONSTRAINT `pk_entities_residue_types` PRIMARY KEY(entity_id, residue_type_id),
	CONSTRAINT `fk_ert_entity_id`
			FOREIGN KEY (entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE,
	CONSTRAINT `fk_ert_residue_type_id`
			FOREIGN KEY (residue_type_id) REFERENCES residue_types (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `signup_requests_residue_types` (
	`signup_request_id` varchar(36) NOT NULL,
	`residue_type_id` int(11) NOT NULL,

	CONSTRAINT `pk_signup_requests_residue_types` PRIMARY KEY (signup_request_id, residue_type_id),
	CONSTRAINT `fk_srrt_signup_request_id`
			FOREIGN KEY (signup_request_id) REFERENCES signup_requests (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE,
	CONSTRAINT `fk_srrt_residue_type_id`
			FOREIGN KEY (residue_type_id) REFERENCES residue_types (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS business_groups;
DROP TABLE IF EXISTS business_type;
DROP TABLE IF EXISTS business_types_groups;
DROP TABLE IF EXISTS entities;
DROP TABLE IF EXISTS entities_residue_types;
DROP TABLE IF EXISTS entity_types;
DROP TABLE IF EXISTS invitations;
DROP TABLE IF EXISTS signup_requests_residue_types;
DROP TABLE IF EXISTS residue_types;
DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS signup_requests;
DROP TABLE IF EXISTS states;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS invitations_entities_id;
SET FOREIGN_KEY_CHECKS=1;
