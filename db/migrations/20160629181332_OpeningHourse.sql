
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `weekdays`;
DROP TABLE IF EXISTS `day_periods`;
DROP TABLE IF EXISTS `entities_weekdays_periods`;
SET FOREIGN_KEY_CHECKS=1;


CREATE TABLE `weekdays` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(250) NOT NULL,

	CONSTRAINT `pk_weekdays_id` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `day_periods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,

  CONSTRAINT `pk_day_periods` PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `entities_weekdays_periods` (
  `entity_id` int(11) NOT NULL,
  `weekday_id` int(11) NOT NULL,
  `day_period_id` int(11) NOT NULL,
  `person_in_charge` varchar(250),
  `phone` varchar(25),

  CONSTRAINT `fk_entity_id`
      FOREIGN KEY (entity_id) REFERENCES entities (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_weekday_id`
      FOREIGN KEY (weekday_id) REFERENCES weekdays (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_day_period_id`
      FOREIGN KEY (day_period_id) REFERENCES day_periods (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS weekdays;
DROP TABLE IF EXISTS day_periods;
DROP TABLE IF EXISTS entities_weekdays_periods;
SET FOREIGN_KEY_CHECKS=1;
