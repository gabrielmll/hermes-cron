
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `entities`
  ADD COLUMN `complement_address` VARCHAR(250) AFTER `neighborhood`;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `entities`
  DROP COLUMN `complement_address`;
