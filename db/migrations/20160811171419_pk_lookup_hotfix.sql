
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `entities_weekdays_periods`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `entities_weekdays_periods`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `entities_weekdays_periods` (
  `entity_id` int(11) NOT NULL,
  `weekday_id` int(11) NOT NULL,
  `day_period_id` int(11) NOT NULL,
  `person_in_charge` varchar(250),
  `phone` varchar(25),
  CONSTRAINT `pk_entities_weekdays_periods` PRIMARY KEY(entity_id, weekday_id, day_period_id),
  CONSTRAINT `fk_entity_id`
      FOREIGN KEY (entity_id) REFERENCES entities (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_weekday_id`
      FOREIGN KEY (weekday_id) REFERENCES weekdays (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_day_period_id`
      FOREIGN KEY (day_period_id) REFERENCES day_periods (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


  --
  -- Table structure for table `day_periods_route_schedule`
  --
  SET FOREIGN_KEY_CHECKS=0;
  DROP TABLE IF EXISTS `day_periods_route_schedule`;
  SET FOREIGN_KEY_CHECKS=1;
  CREATE TABLE `day_periods_route_schedule` (
    `day_period_id` int(11) NOT NULL,
    `route_schedule_id` int(11) NOT NULL,
    CONSTRAINT `pk_day_periods_route_schedule` PRIMARY KEY(day_period_id, route_schedule_id),
    CONSTRAINT `fk_day_period`
      FOREIGN KEY (`day_period_id`)
      REFERENCES day_periods (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    CONSTRAINT `fk_route_schedule`
      FOREIGN KEY (`route_schedule_id`)
      REFERENCES route_schedules (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  --
  -- Table structure for table `container_type_collection`
  --
  SET FOREIGN_KEY_CHECKS=0;
  DROP TABLE IF EXISTS `container_type_collection`;
  SET FOREIGN_KEY_CHECKS=1;
  CREATE TABLE `container_type_collection` (
    `container_type_id` int(11) NOT NULL,
    `collection_request_id` int(11) NOT NULL,
    `filled_percentage_requested` int(3),
    `filled_percentage_collected` int(3),
    CONSTRAINT `pk_container_type_collection` PRIMARY KEY(container_type_id, collection_request_id),
    CONSTRAINT `fk_container_type`
      FOREIGN KEY (`container_type_id`)
      REFERENCES waste_container_types (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    CONSTRAINT `fk_collection_request`
      FOREIGN KEY (`collection_request_id`)
      REFERENCES collection_requests (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
