
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `src_dst_entities`;
DROP TABLE IF EXISTS `waste_container_types`;
DROP TABLE IF EXISTS `container_dst_entity`;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `src_dst_entities` (
  `src_entity_id` int(11) NOT NULL,
  `dst_entity_id` int(11) NOT NULL,
  `invitation_id` varchar(36) NOT NULL,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT `pk_src_dst_entities` PRIMARY KEY(src_entity_id, dst_entity_id, invitation_id),
  CONSTRAINT `fk_src_entity_id`
    FOREIGN KEY (src_entity_id) REFERENCES entities(id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_dst_src_entity_id`
      FOREIGN KEY (dst_entity_id) REFERENCES entities (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_invitation_id`
      FOREIGN KEY (invitation_id) REFERENCES invitations (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `waste_container_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_entity_id` int(11) NOT NULL,
  `residue_type_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
	`capacity` int(11) NOT NULL,

	CONSTRAINT `pk_waste_container_types` PRIMARY KEY (id),
  CONSTRAINT `fk_owner_entity_id`
      FOREIGN KEY (owner_entity_id) REFERENCES entities (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_residue_type_id`
      FOREIGN KEY (residue_type_id) REFERENCES residue_types (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `container_dst_entity` (
  `dst_entity_id` int(11) NOT NULL,
  `waste_container_id` int(11) NOT NULL,
  `container_amount` int(11) NOT NULL,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT `pk_container_dst_entity` PRIMARY KEY(dst_entity_id, waste_container_id),
  CONSTRAINT `fk_container_dst_entity_id`
      FOREIGN KEY (dst_entity_id) REFERENCES entities (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT `fk_waste_container_id`
      FOREIGN KEY (waste_container_id) REFERENCES waste_container_types (id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `src_dst_entities`;
DROP TABLE IF EXISTS `waste_container_types`;
DROP TABLE IF EXISTS `container_dst_entity`;
SET FOREIGN_KEY_CHECKS=1;
