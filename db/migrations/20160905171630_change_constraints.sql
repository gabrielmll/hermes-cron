
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `entities_residue_types`
  DROP FOREIGN KEY `fk_ert_entity_id`;

ALTER TABLE `entities_residue_types`
  ADD CONSTRAINT `fk_ert_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `invitations`
  DROP FOREIGN KEY `fk_invitations_dst_entity_id`;

ALTER TABLE `invitations`
  DROP FOREIGN KEY `fk_invitations_src_entity_id`;

ALTER TABLE `invitations`
  ADD CONSTRAINT `fk_invitations_dst_entity_id` FOREIGN KEY (`dst_entity_id`) REFERENCES `entities` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `invitations`
  ADD CONSTRAINT `fk_invitations_src_entity_id` FOREIGN KEY (`src_entity_id`) REFERENCES `entities` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `users`
  DROP FOREIGN KEY `fk_users_entity_id`;

ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_entity_id` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
