
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE invitations;

CREATE TABLE `invitations` (
	`id` varchar(36) NOT NULL,
	`src_entity_id` int(11) NOT NULL,
	`dst_entity_id` int(11) NOT NULL,
	`state_id` int(11) NOT NULL,
	`exp_date` datetime DEFAULT 0,
	`created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

	CONSTRAINT `pk_invitations_id` PRIMARY KEY (id),
	CONSTRAINT `fk_invitations_src_entity_id`
			FOREIGN KEY (src_entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_invitations_dst_entity_id`
			FOREIGN KEY (dst_entity_id) REFERENCES entities (id)
			ON DELETE RESTRICT
			ON UPDATE RESTRICT,
	CONSTRAINT `fk_invitations_state_id`
		FOREIGN KEY (state_id) REFERENCES states (id)
			ON DELETE RESTRICT
			ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
