
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE entities
  ADD `company_name` VARCHAR(250) AFTER `name`,
  ADD `state_inscription` VARCHAR(12) AFTER `br_cpf_cnpj`;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE entities
  DROP COLUMN `company_name`,
  DROP COLUMN `state_inscription`;
