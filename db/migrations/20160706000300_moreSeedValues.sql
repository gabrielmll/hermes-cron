
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
INSERT INTO `entities` (id, entity_type_id, business_type_id, name, email, phone, street_address, street_number,
postal_code, city, state, country, br_cpf_cnpj, score)
VALUES (1, 1, 7, "Residuall", "contato@residuall.org", "+5131994689926", "Rua Flávio dos Santos", "372", "31015150",
"Belo Horizonte", "MG", "Brasil", "23270730008000109", 420);
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `weekdays` WRITE;
/*!40000 ALTER TABLE `weekdays` DISABLE KEYS */;
INSERT INTO `weekdays`
VALUES (1, "Segunda-feira"),(2, "Terça-feira"),(3, "Quarta-feira"),(4, "Quinta-feira"),(5, "Sexta-feira"),(6, "Sábado"),(7, "Domingo");
/*!40000 ALTER TABLE `weekdays` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `day_periods` WRITE;
/*!40000 ALTER TABLE `day_periods` DISABLE KEYS */;
INSERT INTO `day_periods`
VALUES (1, "Manhã"),(2, "Tarde"),(3, "Noite");
/*!40000 ALTER TABLE `day_periods` ENABLE KEYS */;
UNLOCK TABLES;

DELETE FROM `roles`;
LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Master Account','Contas da Residuall'),(2,'Administrador','Administrador da empresa'),(3,'Gestor','Gestor designado pelo Administrador.'),(4,'Operador','Operador da empresa');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DELETE FROM `entities` WHERE id=1;

DELETE FROM `weekdays`;

DELETE FROM `day_periods`;

DELETE FROM `roles`;
LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador','Administrador da empresa'),(2,'Gestor','Gestor designado pelo Administrador.'),(3,'Operador','Operador da empresa');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
