
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE signup_requests ADD has_collector bool;


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE signup_requests DROP COLUMN has_collector;

