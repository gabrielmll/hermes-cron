

-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Removing previous tables that are not to be used by now
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `feedback_types`;
DROP TABLE IF EXISTS `feedback_registrations`;
DROP TABLE IF EXISTS `warning_types`;
DROP TABLE IF EXISTS `entities_warnings`;
SET FOREIGN_KEY_CHECKS=1;

--
-- Table structure for table `collection_requests`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `collection_requests`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `collection_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `generator_entity_id` int(11) NOT NULL,
  `collector_entity_id` int(11) NOT NULL,
  `residue_type_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `emergency_type` tinyint(1) NOT NULL,
  `amount_requested` int(11),
  `score` int(3) NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `pk_collection_requests` PRIMARY KEY (`id`),
  CONSTRAINT `fk_generator_request`
    FOREIGN KEY (`generator_entity_id`)
    REFERENCES entities (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_collector_request`
    FOREIGN KEY (`collector_entity_id`)
    REFERENCES entities (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_residue_request`
    FOREIGN KEY (`residue_type_id`)
    REFERENCES residue_types(`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_state_request`
    FOREIGN KEY (`state_id`)
    REFERENCES states(`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `container_type_collection`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `container_type_collection`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `container_type_collection` (
  `container_type_id` int(11) NOT NULL,
  `collection_request_id` int(11) NOT NULL,
  `filled_percentage_requested` int(3),
  `filled_percentage_collected` int(3),
  CONSTRAINT `fk_container_type`
    FOREIGN KEY (`container_type_id`)
    REFERENCES waste_container_types (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_collection_request`
    FOREIGN KEY (`collection_request_id`)
    REFERENCES collection_requests (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `route_schedules`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `route_schedules`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `route_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collector_entity_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `driver_user_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `date` DATE NOT NULL,
  `load_capacity` int(5),
  `current_load` int(5),
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `pk_route_schedules` PRIMARY KEY (`id`),
  CONSTRAINT `fk_entity_route`
    FOREIGN KEY (`collector_entity_id`)
    REFERENCES entities (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vehicle_route`
    FOREIGN KEY (`vehicle_id`)
    REFERENCES vehicles (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_driver_route`
    FOREIGN KEY (`driver_user_id`)
    REFERENCES users (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_state_route`
    FOREIGN KEY (`state_id`)
    REFERENCES states(`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------
--
-- Table structure for table `collection_registrations`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `collection_registrations`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `collection_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_schedule_id` int(11) NOT NULL,
  `collection_request_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `amount_collected` int(11),
  `cost` decimal(5,2) NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
	`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `pk_collection_registrations` PRIMARY KEY (`id`),
  CONSTRAINT `fk_route_registrations`
    FOREIGN KEY (`route_schedule_id`)
    REFERENCES route_schedules(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_request_registrations`
    FOREIGN KEY (`collection_request_id`)
    REFERENCES collection_requests(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_state_registrations`
    FOREIGN KEY (`state_id`)
    REFERENCES states(`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `day_periods_route_schedule`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `day_periods_route_schedule`;
SET FOREIGN_KEY_CHECKS=1;
CREATE TABLE `day_periods_route_schedule` (
  `day_period_id` int(11) NOT NULL,
  `route_schedule_id` int(11) NOT NULL,
  CONSTRAINT `fk_day_period`
    FOREIGN KEY (`day_period_id`)
    REFERENCES day_periods (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_route_schedule`
    FOREIGN KEY (`route_schedule_id`)
    REFERENCES route_schedules (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS collection_requests;
DROP TABLE IF EXISTS container_type_collection;
DROP TABLE IF EXISTS route_schedules;
DROP TABLE IF EXISTS collection_registrations;
DROP TABLE IF EXISTS day_periods_route_schedule;
DROP TABLE IF EXISTS feedback_types;
DROP TABLE IF EXISTS feedback_registrations;
DROP TABLE IF EXISTS warning_types;
DROP TABLE IF EXISTS entities_warnings;
SET FOREIGN_KEY_CHECKS=1;
