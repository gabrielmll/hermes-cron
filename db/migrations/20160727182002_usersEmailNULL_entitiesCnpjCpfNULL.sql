
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE users MODIFY email varchar(100) NOT NULL;
ALTER TABLE entities MODIFY br_cpf_cnpj varchar(14) NULL;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE users MODIFY email varchar(100) NULL;
ALTER TABLE entities MODIFY br_cpf_cnpj varchar(14) NOT NULL;
