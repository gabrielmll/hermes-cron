
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied


LOCK TABLES `business_groups` WRITE;
/*!40000 ALTER TABLE `business_groups` DISABLE KEYS */;
INSERT INTO `business_groups` VALUES (1,'Estabelecimentos Alimentícios','Estabelecimentos de consumo de produtos alimentício.');
/*!40000 ALTER TABLE `business_groups` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `business_types` WRITE;
/*!40000 ALTER TABLE `business_types` DISABLE KEYS */;
INSERT INTO `business_types` VALUES (1,'Churrascaria',NULL),(2,'Pizzaria',NULL),(3,'Cafeteria / Confeitaria',NULL),(4,'Padaria',NULL),(5,'Bares / Botecos',NULL),(6,'Cozinha Brasileira',NULL),(7,'Cozinha Japonesa',NULL),(8,'Cozinha Italiana',NULL),(9,'Cozinha Chinesa',NULL),(10,'Cozinha Árabe',NULL),(11,'Cozinha Internacional',NULL),(12,'Cozinha Natural / Vegana',NULL),(13,'Self-service',NULL),(14,'Lanchonete / Fast-food',NULL);
/*!40000 ALTER TABLE `business_types` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `business_types_groups` WRITE;
/*!40000 ALTER TABLE `business_types_groups` DISABLE KEYS */;
INSERT INTO `business_types_groups` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1);
/*!40000 ALTER TABLE `business_types_groups` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `entity_types` WRITE;
/*!40000 ALTER TABLE `entity_types` DISABLE KEYS */;
INSERT INTO `entity_types` VALUES (1,'Residuall','A melhor startup do universo.'),(2,'Empresa de Coleta','Empresas de coleta de resíduos.'),(3,'Gerador de Resíduos','Entidades que geram resíduos.');
/*!40000 ALTER TABLE `entity_types` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `residue_types` WRITE;
/*!40000 ALTER TABLE `residue_types` DISABLE KEYS */;
INSERT INTO `residue_types` VALUES (1,'Resíduos dos serviços de saúde',''),(2,'Óleos de motores, transmissões e lubrificação usados ou contaminados',''),(3,'Óleo Vegetal usado',''),(4,'Óleos hidráulicos usados',''),(5,'Plástico',''),(6,'Alumínio',''),(7,'Óleos isolantes, de refrigeração e de transmissão de calor usados',''),(8,'Lodos do tratamento local de efluentes',''),(9,'Chumbo',''),(10,'Resíduos Químicos (contempla resíduos de ácidos, bases, sais, farmaceuticos, fertilizantes, etc.)',''),(11,'Aparas de matérias plásticas',''),(12,'Absorventes, materiais filtrantes, panos de limpeza e vestuário de proteção',''),(13,'Níquel',''),(14,'Resíduos Orgânicos',''),(15,'Resíduos de equipamento elétrico e eletrônico',''),(16,'Resíduos de combustíveis líquidos',''),(17,'Resíduos da refinação de petróleo',''),(18,'Escórias do forno',''),(19,'Óleos bunker usados de navios',''),(20,'Embalagens de papel e cartão',''),(21,'Aparas e limalhas de metais',''),(22,'Ferro e aço',''),(23,'Estanho',''),(24,'Cobre, bronze e latão',''),(25,'Pilhas, baterias e acumuladores elétricos:',''),(26,'Zinco',''),(27,'Resíduos da produção de bebidas alcoólicas e não alcoólicas (excluindo café, chá e cacau)',''),(28,'Vidro',''),(29,'Resíduos retirados da fase de gradeamento',''),(30,'Resíduos de processos térmicos',''),(31,'Pneus',''),(32,'Sucatas metálicas',''),(33,'Resíduos da agricultura, horticultura, aquicultura, silvicultura, caça e pesca',''),(34,'Resíduos de instalações de gestão de resíduos, de estações de tratamento de águas residuais e da preparação de água para consumo humano e água para consumo industrial:',''),(35,'Resíduos da transformação física e química de minérios',''),(36,'Resíduos da indústria do couro e produtos de couro e da indústria têxtil',''),(37,'Resíduos da mineração',''),(38,'Resíduos de construção e demolição (incluindo solos escavados de locais contaminados):',''),(39,'Madeira',''),(40,'Resíduos de tecidos animais',''),(41,'Resíduos do processamento de madeira e fabricação de painéis e mobiliário',''),(42,'Resíduos de processos de galvanização',''),(43,'Resíduos da indústria do ferro e do aço',''),(44,'Embalagens longa-vida',''),(45,'Resíduos da preparação e processamento de carne, peixe e outros produtos alimentares de origem animal',''),(46,'Tijolos',''),(47,'Resíduos de tratamentos químicos de superfície e revestimentos de metais e outros materiais (galvanização, zincagem, decapagem, contrastação, fosfatação, desengorduramento alcalino, anodização)',''),(48,'Ladrilhos, telhas e materiais cerâmicos',''),(49,'Resíduos da fabricação do vidro e de produtos de vidro',''),(50,'Resíduos de solventes, fluidos de refrigeração e gases propulsores de espumas/ aerossóis orgânicos',''),(51,'Resíduos de alumina',''),(52,'Resíduos da fabricação de peças cerâmicas, tijolos, ladrilhos, telhas e produtos de construção',''),(53,'Tecidos',''),(54,'Resíduos vitrificados e resíduos da vitrificação:',''),(55,'Resíduos de cimento e de lodos de cimento',''),(56,'Resíduos de tecidos vegetais','');
/*!40000 ALTER TABLE `residue_types` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador','Administrador da empresa'),(2,'Gestor','Gestor designado pelo Administrador.'),(3,'Operador','Operador da empresa');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'STATE_WAITING_AUTHORIZATION','Aguardando autorização da Residuall.');
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
