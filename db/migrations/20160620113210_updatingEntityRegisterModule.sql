
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

--
-- Table structure for table `vehicles`
--
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `vehicles`;
SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `license_plate` char(7) NOT NULL,
  `name` char(45) NOT NULL,
  `load_capacity` int(11) DEFAULT 0 NOT NULL,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT `pk_vehicles_id` PRIMARY KEY (`id`),
  CONSTRAINT `fk_vehicle_entity_id`
    FOREIGN KEY (`entity_id`) REFERENCES entities (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vehicle_state_id`
    FOREIGN KEY (`state_id`) REFERENCES states (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS vehicles;
SET FOREIGN_KEY_CHECKS=1;
