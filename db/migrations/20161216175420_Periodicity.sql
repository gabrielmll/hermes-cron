
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

CREATE TABLE `periodicity` (
	`collector_entity_id` int(11) NOT NULL,
	`generator_entity_id` int(11) NOT NULL,
	`residue_type_id` int(11) NOT NULL,
	`day_periods` smallint NOT NULL,
	`repetition` enum('weekly', 'monthly', 'yearly') DEFAULT NULL,
	`sunday` varchar(20) DEFAULT NULL,
	`monday` varchar(20) DEFAULT NULL,
	`tuesday` varchar(20) DEFAULT NULL,
	`wednesday` varchar(20) DEFAULT NULL,
	`thursday` varchar(20) DEFAULT NULL,
	`friday` varchar(20) DEFAULT NULL,
	`saturday` varchar(20) DEFAULT NULL,
	`repeats_every` smallint NOT NULL,
	`start_date` date NOT NULL,
	`end_repetition` smallint DEFAULT NULL,
	`end_date` date DEFAULT NULL,
	`days_offset` int(11) DEFAULT NULL,

	CONSTRAINT `pk_periodicity`
		PRIMARY KEY (collector_entity_id, generator_entity_id, residue_type_id),
	CONSTRAINT `fk_periodicity_col_entity`
		FOREIGN KEY (collector_entity_id) REFERENCES entities (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT `fk_periodicity_gen_entity`
		FOREIGN KEY (generator_entity_id) REFERENCES entities (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT `fk_periodicity_residue_type`
		FOREIGN KEY (residue_type_id) REFERENCES residue_types (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE collection_requests ADD COLUMN scheduled_date date DEFAULT NULL
	AFTER score;


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE `periodicity`;
ALTER TABLE collection_requests DROP COLUMN scheduled_date;

