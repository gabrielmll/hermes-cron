
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

ALTER TABLE `residue_types`
  ADD COLUMN IF NOT EXISTS `unit` varchar(3);

-- ------------------------------------------------------

ALTER TABLE `day_periods`
  ADD COLUMN IF NOT EXISTS `time_init` TIME,
  ADD COLUMN IF NOT EXISTS `time_end` TIME;

-- ------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `entities`
  ADD COLUMN IF NOT EXISTS `neighborhood` varchar(100) AFTER `postal_code`,
  ADD COLUMN IF NOT EXISTS `state_id` int(11) NOT NULL AFTER `business_group_id`,
  ADD CONSTRAINT `fk_entity_state`
    FOREIGN KEY (state_id) REFERENCES states (id)
      ON DELETE RESTRICT
      ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

-- ------------------------------------------------------

DELETE FROM `weekdays`;

LOCK TABLES `weekdays` WRITE;
/*!40000 ALTER TABLE `weekdays` DISABLE KEYS */;
INSERT INTO `weekdays`
VALUES (1, "Domingo"),(2, "Segunda-feira"),(3, "Terça-feira"),(4, "Quarta-feira"),(5, "Quinta-feira"),(6, "Sexta-feira"),(7, "Sábado");
/*!40000 ALTER TABLE `weekdays` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------

UPDATE `day_periods`
  SET `time_init`="08:00:00", `time_end`="12:00:00"
  WHERE id=1;

UPDATE `day_periods`
  SET `time_init`="12:00:00", `time_end`="18:00:00"
  WHERE id=2;

UPDATE `day_periods`
  SET `time_init`="18:00:00", `time_end`="22:00:00"
  WHERE id=3;

-- ------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM `business_types_groups`;
DELETE FROM `business_groups`;
SET FOREIGN_KEY_CHECKS=1;

LOCK TABLES `business_groups` WRITE;
/*!40000 ALTER TABLE `business_groups` DISABLE KEYS */;
INSERT INTO `business_groups` VALUES (1,'Estabelecimentos Alimentícios','Estabelecimentos de consumo de produtos alimentício.');
INSERT INTO `business_groups`(id,name) VALUES (2,'Serviços de Saúde'),(3,'Hospedagens'),(4,'Indústrias');
/*!40000 ALTER TABLE `business_groups` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM `business_types`;
SET FOREIGN_KEY_CHECKS=1;

LOCK TABLES `business_types` WRITE;
/*!40000 ALTER TABLE `business_types` DISABLE KEYS */;
INSERT INTO `business_types` VALUES ('1','Bar / Boteco / Pub',NULL),('2','Cafeteria / Confeitaria',NULL),
('3','Churrascaria',NULL),('4','Cozinha Árabe',NULL),('5','Cozinha Brasileira',NULL),
('6','Cozinha Chinesa',NULL),('7','Cozinha Internacional',NULL),('8','Cozinha Italiana',NULL),
('9','Cozinha Japonesa',NULL),('10','Cozinha Mineira',NULL),('11','Cozinha Natural / Vegana',NULL),
('12','Lanchonete / Fast-food',NULL),('13','Padaria',NULL),('14','Pizzaria',NULL),
('15','Self-service',NULL),('16','Outros','Outros negócios alimentícios'),
('17','Centros de controle de zoonoses','Centros de controle de zoonoses'),
('18','Clínicas médicas e odontológicas','Serviços de medicina legal'),
('19','Clínicas veterinárias','Centros de controle de zoonoses'),
('20','Distribuidores de produtos farmacêuticos','Distribuidores de produtos farmacêuticos'),
('21','Estabelecimentos de ensino e pesquisa na área de saúde','Estabelecimentos de ensino e pesquisa na área de saúde'),
('22','Farmácias e drogarias','Drogarias e farmácias inclusive as de manipulação'),
('23','Hospitais','Serviços de medicina legal'),
('24','Importadores, distribuidores e produtores de materiais e controles para diagnóstico in vitro','Importadores, distribuidores e produtores de materiais e controles para diagnóstico in vitro'),
('25','Laboratórios analíticos de produtos para saúde','Laboratórios analíticos de produtos para saúde'),
('27','Serviços de acupuntura','Serviços de acupuntura e serviços de tatuagem'),
('26','Necrotérios, funerárias e serviços onde se realizem atividades de embalsamamento','Necrotérios, funerárias e serviços onde se realizem atividades de embalsamamento'),
('28','Serviços de tatuagem','Serviços de acupuntura e serviços de tatuagem'),
('29','Unidades móveis de atendimento à saúde','Unidades móveis de atendimento à saúde'),
('30','Outros','Outros serviços de saúde'),('31','Albergue / Hostel',NULL),('32','Hotel',NULL),('33','Resort',NULL),
('34','Pousada',NULL),('35','Resort',NULL),('36','Outros','Outros serviços de hospedagens'),('37','Refeitório',NULL),('38','Outros','Outros serviços industriais');
/*!40000 ALTER TABLE `business_types` ENABLE KEYS */;
UNLOCK TABLES;

-- ------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM `business_types_groups`;
SET FOREIGN_KEY_CHECKS=1;

LOCK TABLES `business_types_groups` WRITE;
/*!40000 ALTER TABLE `business_types_groups` DISABLE KEYS */;
INSERT INTO `business_types_groups` (`business_group_id`, `business_type_id`) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),
(1,12),(1,13),(1,14),(1,15),(1,16),(2,17),(2,18),(2,19),(2,20),(2,21),(2,22),(2,23),(2,24),(2,25),(2,26),
(2,27),(2,28),(2,29),(2,30),(3,31),(3,32),(3,33),(3,34),(3,35),(3,36),(4,37),(4,38);
/*!40000 ALTER TABLE `business_types_groups` ENABLE KEYS */;
UNLOCK TABLES;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

ALTER TABLE `residue_types`
  DROP COLUMN IF EXISTS `unit`;

ALTER TABLE `day_periods`
  DROP COLUMN IF EXISTS `time_init`,
  DROP COLUMN IF EXISTS `time_end`;

DELETE FROM `business_groups` WHERE id=2;
DELETE FROM `business_groups` WHERE id=3;
DELETE FROM `business_groups` WHERE id=4;
